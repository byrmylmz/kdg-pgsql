SELECT * FROM employees;

SELECT NOW();
SELECT CURRENT_DATE;

SELECT NOW(), CURRENT_DATE;

SELECT NOW() "TIME TODAY",
       CURRENT_DATE today,
       employee_id id,
       birth_date,
       'TEST' AS text_data

FROM employees;

SELECT NOW() "TIME TODAY",
       CURRENT_DATE today,
       employee_id id,
       birth_date,
       'TEST' AS text_data,
       salary AS without_increase,
       salary*1.1 raise


FROM employees;




