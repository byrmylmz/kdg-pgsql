/*
    Exercise at class
    https://docs.google.com/document/d/1eZcmjRKes-ISp1o6KcDpkehivd9ytOYE/edit
*/

/*
    Make the following change to the EMPLOYEES table:
*/

UPDATE EMPLOYEES
SET location='maastricht'
WHERE employee_id='9995555';

--1

SELECT * FROM projects;

--2
SELECT project_name,department_id FROM projects;

--3a
SELECT 'project' "?column?", department_id, 'is handled by' "?column?", project_id FROM  projects ORDER BY project_id DESC ;

--3b

SELECT 'project' || ' ' || project_id || ' ' || 'is handled by' ||' '|| department_id FROM projects  ;
--SEE HERE NULL BORKEN ALL
SELECT 'project' || ' ' || NULL || ' ' || 'is handled by' ||' '|| department_id FROM projects  ;

-- BETTER VERSION
SELECT CONCAT( 'project' , ' ' , project_id , ' ' , 'is handled by' ,' ', department_id) FROM projects  ;
-- MORE EASY AND BETTER

--CONCAT WITH SEPERATOR more resilliant
SELECT CONCAT_WS(' ','project' , project_id, 'is handled by', department_id) FROM projects  ;


--4
SELECT current_date - birth_date,(current_date - birth_date)/365, age(birth_date)
FROM FAMILY_MEMBERS;

--5a
SELECT employee_id,project_id,hours;
SELECT * FROM TASK;
SELECT department_id, manager_id, start_date FROM DEPARTMENTS;

--5b
SELECT last_name, salary department_id FROM EMPLOYEES; --comma

--6

SELECT DISTINCT location "city" FROM employees;

SELECT DISTINCT location city FROM employees;

SELECT DISTINCT UPPER(location) city FROM employees;

SELECT DISTINCT INITCAP(location) city FROM employees; -- INIT CAP JUST FOR WORD NOT SENTENCE

--7
SELECT DISTINCT department_id, INITCAP(location) city FROM employees;

--8a
SELECT NOW();
SELECT CURRENT_TIMESTAMP;
SELECT CURRENT_DATE;

--8b
SELECT 150*0.85 calculation;
SELECT 150*(1-0.15) calculation;
SELECT 150*(85/100) calculation; -- niye calismadi


--8c
SELECT CONCAT_WS(' ','SQL','Data Retrieval','Chapter 3-4') "Best course"

--9

SELECT * FROM employees WHERE employee_id='999111111';

---10 11 at home

--12
SELECT *
FROM tasks
WHERE hours BETWEEN 20 AND 35 AND project_id=10;

--14 first way

SELECT employee_id,last_name,province
FROM employees
WHERE province='GR' OR province='NB' ORDER BY last_name ;

--14 second way

SELECT employee_id,last_name,province
FROM employees
WHERE province IN ('GR','NB') ORDER BY last_name ;

--15

SELECT department_id,first_name
FROM employees
WHERE first_name IN ('Suzan','Martina','Henk') OR first_name='Douglas' ORDER BY first_name;
;

--16
SELECT last_name,salary,department_id
FROM employees
WHERE department_id='7' AND salary < 40000 OR employee_id='999666666'
;

--17


SELECT *
FROM employees;


--21
SELECT first_name, last_name, salary
FROM employees
ORDER BY salary ASC
FETCH NEXT 1 ROW  WITH TIES;

SELECT first_name, last_name, salary
FROM employees
ORDER BY salary ASC
    FETCH NEXT 1 ROW ONLY;


--22
SELECT first_name, last_name, birth_date
FROM employees
ORDER BY birth_date ASC
    FETCH NEXT 3 ROW  WITH TIES;

--23

SELECT employee_id, project_id, hours
FROM tasks
WHERE hours IS NOT NULL
ORDER BY hours DESC
OFFSET 3 ROWS
    FETCH NEXT 3 ROWS ONLY
;




