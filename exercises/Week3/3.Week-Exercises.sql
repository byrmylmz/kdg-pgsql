-- link https://docs.google.com/document/d/10etL0Eel_GRSj3FqJaltElwtK5WBH0Uq/edit

-- 1a


-- 1b
SELECT d.department_id, department_name, project_id, project_name, location
FROM DEPARTMENTS d
JOIN PROJECTS p ON p.department_id=d.department_id
ORDER BY 1,3
;

--2

SELECT d.department_id,d.manager_id, e.first_name, e.salary,e.parking_spot
FROM DEPARTMENTS d
JOIN EMPLOYEES e ON d.manager_id = e.employee_id
ORDER BY parking_spot ASC;


--3

SELECT p.project_name, e.location, concat_ws(' ', e.first_name, e.last_name), d.department_id
FROM EMPLOYEES e
         JOIN TASKS t ON e.employee_id = t.employee_id
         JOIN PROJECTS p ON p.project_id = t.project_id
         JOIN DEPARTMENTS d ON d.department_id = p.department_id
ORDER BY 4
;

SELECT p.project_name, e.location, concat_ws(' ', e.first_name, e.last_name) AS full_name, p.department_id
FROM EMPLOYEES e
          JOIN PROJECTS p ON e.department_id=p.department_id
ORDER BY 4

;

--4
SELECT p.project_name,
       e.location, concat_ws(' ', e.first_name, e.last_name) AS full_name,
       e.department_id
FROM EMPLOYEES e
         JOIN tasks t ON (e.employee_id = t.employee_id)
         JOIN PROJECTS p ON t.project_id = p.project_id
WHERE LOWER(p.location) = 'eindhoven'
ORDER BY 4, e.last_name
;


SELECT p.project_name,
       p.location,
       CONCAT_WS(' ', e.first_name, e.last_name) AS full_name,
       e.department_id
FROM employees e
         JOIN tasks t ON (e.employee_id = t.employee_id)
         JOIN projects p ON (t.project_id = p.project_id)
WHERE LOWER(p.location) = 'eindhoven'
ORDER BY 4, e.last_name;


-- hoca sonununda bunu yazdi
SELECT p.project_name,
       p.location,
       CONCAT_WS(' ', e.first_name,e.last_name) AS full_name,
       e.department_id,
       d.*
FROM employees e
         JOIN tasks t ON (e.employee_id = t.employee_id)
         JOIN projects p ON (t.project_id = p.project_id)
         JOIN departments d ON (p.department_id = d.department_id)
WHERE LOWER(p.location) = 'eindhoven'
   OR LOWER(d.department_name) = 'administration'
ORDER BY 4,e.last_name;


-- lets think a little
SELECT  e.employee_id,e.last_name,p.project_name,
        t.project_id,t.hours
FROM EMPLOYEES e
         JOIN TASKS t ON e.employee_id=t.employee_id
         JOIN PROJECTS  p ON t.project_id=p.project_id;


--trende yaptim
SELECT mgr.last_name
FROM public.employees e
JOIN employees mgr ON (e.manager_id=mgr.employee_id)
WHERE e.employee_id='999444444'
;


--12

