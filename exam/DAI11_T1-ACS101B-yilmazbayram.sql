--**************************************
--Combo exercises week 3 - test in class
--**************************************

------------
--Exercise 2
------------

--For employees with family members, provide the requested family information AND information from their boss.
--Only records for which the manager was born after January 1985 and only the female relatives.
--Beware of aliases and the formatting of certain fields.
--Sort by employee's last name.

/*
+-------------+-----+------------+------+-----------+-------------------+---------+----------+
|employee name|name |relationship|gender|employee_id|first_name_capitals|upper    |birthdate |
+-------------+-----+------------+------+-----------+-------------------+---------+----------+
|Douglas Bock |Mary |PARTNER     |F     |999444444  |WILLEM             |ZUIDERWEG|1985-08-12|
|Douglas Bock |Diana|DAUGHTER    |F     |999444444  |WILLEM             |ZUIDERWEG|1985-08-12|
+-------------+-----+------------+------+-----------+-------------------+---------+----------+
*/

--ANSWER EXERCISE 2
SELECT  e.first_name || ' ' || e.last_name as employee_name,f.name,f.relationship,f.gender,f.employee_id,upper(e.first_name) as first_name_capital,Upper(e.last_name),e.birth_date
    FROM employees e
    JOIN family_members f ON e.employee_id = f.employee_id
    where e.birth_date <'1-JAN-1985';

;

